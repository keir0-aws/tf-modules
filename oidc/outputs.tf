output "ROLE_ARN" {
  description = "INFRA Role that needs to be assumed by GitLab CI"
  value       = aws_iam_role.gitlab_ci.arn
}
