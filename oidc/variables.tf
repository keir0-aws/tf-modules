variable "gitlab_url" {
  type        = string
  default     = "https://gitlab.com"
  description = "GitLab URL"
}

variable "aud_value" {
  type        = string
  default     = "https://gitlab.com"
  description = "GitLab Aud"
}
variable "match_field" {
  type        = string
  default     = "aud"
  description = "GitLab match_field"
}

variable "match_value" {
  type        = list(any)
  description = "GitLab match_value"

}

variable "managed_policy_names" {
  type        = list(any)
  description = "IAM managed_policy_names"
}

variable "name" {
  description = "IAM Name Suffix"
  type        = string
}
